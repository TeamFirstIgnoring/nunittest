﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace User1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application. Changed BY now
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles(); 
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
